from fastapi.testclient import TestClient
from main import app
import pytest

client = TestClient(app)

def test_login():
    response = client.post(
        '/login', data={"username": "user1", "password": "password1"})
    assert response.status_code == 200