from fastapi import FastAPI, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from datetime import datetime, timedelta
from jose import jwt, JWTError

app = FastAPI()

users = {
    "user1": {
        "password": "password1",
        "salary": 5000,
        "raise": "15.12"
    },
    "user2": {
        "password": "password2",
        "salary": 5001,
        "raise": "17.05"
    }
}

# random secret key
key = "3149501"

expiration = 5 # mins

oauth2Scheme = OAuth2PasswordBearer(tokenUrl="/login")


def getUser(username: str):
    if username in users:
        return users[username]
    return None

async def getCurrentUser(token: str = Depends(oauth2Scheme)):
    try:
        payload = jwt.decode(token, key, algorithms=["HS256"])
        username = payload["sub"]
        user = getUser(username)
        if user is None:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="Invalid authentication credentials",
                headers={"WWW-Authenticate": "Bearer"},
            )
        return user
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )


@app.post("/login")
async def login(data: OAuth2PasswordRequestForm = Depends()):
    username = data.username
    password = data.password
    if not username in users and password == users[username]["password"]:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    tokenExpiresIn = timedelta(minutes=expiration)
    encodedJWT = jwt.encode({"sub": username, "exp": datetime.utcnow() + tokenExpiresIn}, key, algorithm="HS256")
    return {"accessToken": encodedJWT, "token_type": "bearer"}


@app.get("/salary")
async def getSalary(user: dict = Depends(getCurrentUser)):
    return {"salary": user["salary"], "raise": user["raise"]}
